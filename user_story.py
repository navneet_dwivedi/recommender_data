import time
from datetime import timedelta
import datetime
import subprocess
import os


def impressions(table_name,formatted_previousDate,currentDate):
    #cmd = 'bq query --nosync --destination_table=openx.ecpmPlaDataTemp%s "SELECT count(*) as impressions,0 as clicks,0 as firstclicks, 0 as conversion,dateTime,affiliateId,userTrackingCookie,campaignId,productId FROM [userstory.userstory] where event = 99991 and productId is NOT NULL and dateTime>"%s 18:29:59"  and dateTime<"%s 18:29:59" group by  affiliateId,userTrackingCookie,campaignId,productId,dateTime"' %s (table_name)
    query ="SELECT count(*) as impressions,0 as clicks,0 as firstclick, 0 as conversion,dateTime,affiliateId,userTrackingCookie,campaignId,productId FROM [userstory.userstory] where event = 99991 and productId is NOT NULL  and productId != ''  and dateTime>'%s 18:29:59' and dateTime<'%s 18:29:59' group by  affiliateId,userTrackingCookie,campaignId,productId,dateTime" % (formatted_previousDate,currentDate)
    cmd = 'bq query --allow_large_results --nosync --destination_table=ecpm_pla_data.ecpmPlaDataImpression%s' % (table_name) + ' "%s"' % (query)
    print cmd
    proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    print "program output:", out
    jobid = out.split(' ')
    print jobid[3]

    #jobid = 'tyroobackup:bqjob_r2c187d5bb5c953d5_00000155abd55c13_1'
    while True:
        cmd = 'bq show --format=prettyjson -j %s| awk '"'{print $2}'"' | grep "DONE"' %(jobid[3].rstrip())
        proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        if out:  
            print("job completed")
            #export table to csv on google bucket
            #time.sleep(5)
            cmd ='python export_csv.py tyroobackup ecpm_pla_data ecpmPlaDataImpression%s gs://ecpmpladata/ecpmPlaDataImpression%s.csv' %(table_name,table_name)
            proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            print "program output:", out
            #time.sleep(5)
            #import table from csv to bigquery
            cmd ='bq load --skip_leading_rows=1 openx.ecpm_pla_data gs://ecpmpladata/ecpmPlaDataImpression%s.csv impressions:integer,clicks:integer,firstclick:integer,conversion:integer,dateTime:timestamp,affiliateId:integer,userTrackingCookie:string,campaignId:integer,productId:string' %(table_name)
            proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            print "program output:", out
            break
        else:
            print("job still pending! try again")
            time.sleep( 5 )

def clicks(table_name,formatted_previousDate,currentDate):
    #cmd = 'bq query --nosync --destination_table=openx.ecpmPlaDataTemp%s "SELECT count(*) as impressions,0 as clicks,0 as firstclicks, 0 as conversion,dateTime,affiliateId,userTrackingCookie,campaignId,productId FROM [userstory.userstory] where event = 99991 and productId is NOT NULL and dateTime>"%s 18:29:59"  and dateTime<"%s 18:29:59" group by  affiliateId,userTrackingCookie,campaignId,productId,dateTime"' %s (table_name)
    query ="SELECT 0 as impressions,count(*) as clicks,0 as firstclick, 0 as conversion,dateTime,affiliateId,userTrackingCookie,campaignId,productId FROM [userstory.userstory] where event = 99993 and productId is NOT NULL and productId != '' and dateTime>'%s 18:29:59' and dateTime<'%s 18:29:59' group by  affiliateId,userTrackingCookie,campaignId,productId,dateTime" % (formatted_previousDate,currentDate)
    cmd = 'bq query --allow_large_results --nosync --destination_table=ecpm_pla_data.ecpmPlaDataClicks%s' % (table_name) + ' "%s"' % (query)
    print cmd
    proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    print "program output:", out
    jobid = out.split(' ')
    print jobid[3]

    #jobid = 'tyroobackup:bqjob_r2c187d5bb5c953d5_00000155abd55c13_1'
    while True:
        cmd = 'bq show --format=prettyjson -j %s| awk '"'{print $2}'"' | grep "DONE"' %(jobid[3].rstrip())
        proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        if out:  
            print("job completed")
            #export table to csv on google bucket
            #time.sleep(5)
            cmd ='python export_csv.py tyroobackup ecpm_pla_data ecpmPlaDataClicks%s gs://ecpmpladata/ecpmPlaDataClicks%s.csv' %(table_name,table_name)
            proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            print "program output:", out
            #time.sleep(5)
            #import table from csv to bigquery
            cmd ='bq load --skip_leading_rows=1 openx.ecpm_pla_data gs://ecpmpladata/ecpmPlaDataClicks%s.csv impressions:integer,clicks:integer,firstclick:integer,conversion:integer,dateTime:timestamp,affiliateId:integer,userTrackingCookie:string,campaignId:integer,productId:string' %(table_name)
            proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            print "program output:", out
            break
        else:
            print("job still pending! try again")
            time.sleep( 5 )


if __name__ == '__main__':
    totalDays = 2
    currentDate = time.strftime("%Y-%m-%d")
    #table_name = currentDate +"1"
    #read_date = datetime.datetime.strptime(currentDate, "%Y-%m-%d")
    #previous_day = read_date - datetime.timedelta(days=18)
    #formatted_previous_day = previous_day.strftime('%Y-%m-%d')
    #currentDate = formatted_previous_day

    for i in range(1,totalDays):
        current_date = time.strftime("%Y_%m_%d")
        table_name = '_'+current_date+'_'+str(i)
        print table_name
        read_date = datetime.datetime.strptime(currentDate, "%Y-%m-%d")
        previous_day = read_date - datetime.timedelta(days=1)
        formatted_previous_day = previous_day.strftime('%Y-%m-%d')
        currentDate = formatted_previous_day
        previousDate = datetime.datetime.strptime(currentDate, "%Y-%m-%d") - datetime.timedelta(days=1)
        formatted_previousDate = previousDate.strftime('%Y-%m-%d')
        print(formatted_previousDate)
        print(currentDate)
        impressions(table_name,formatted_previousDate,currentDate)
        clicks(table_name,formatted_previousDate,currentDate)

