import pymongo
from pymongo import MongoClient
import time
from datetime import timedelta
import datetime
import json
import MySQLdb
import argparse
import csv

client = MongoClient('54.169.162.50', 27017)
#client = MongoClient('localhost', 27017)

db =client.openx

container_campaign_id = ["10698","11939","12121","12103","12159","12140","9193","11701","12034","12264","12423","13095","7945","12469","7517","9425","11166"]

ref_campaign = {12126:12239,12224:12239,11939:10698,12331:5149,12256:12851,11166:7517,11701:12034,12230:12159,9425:9193}

import uuid

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from oauth2client.client import GoogleCredentials
from googleapiclient import discovery

# [START stream_row_to_bigquery]
def stream_row_to_bigquery(bigquery, project_id, dataset_id, table_name, data,num_retries=5):

    insert_all_data = {
                    'rows': [{
                                    'json': data,
                                    # Generate a unique id for each row so retries don't accidentally
                                    # duplicate insert
                                    'insertId': str(uuid.uuid4()),
                    }]
    }
    return bigquery.tabledata().insertAll(
                    projectId=project_id,
                    datasetId=dataset_id,
                    tableId=table_name,
                    body=insert_all_data).execute(num_retries=num_retries)   
    # [END stream_row_to_bigquery]



def affiliates_zones() :
    dbSql=MySQLdb.connect(host='52.74.245.229',user='root',passwd='svgx1214tyr004777cm',db='openx')
    c=dbSql.cursor()
    managed ='1'
    delivery ='-1'  
    managed_affiliates={}
    c.execute("""SELECT ox_affiliates.affiliateid, ox_zones.zoneid FROM ox_affiliates JOIN ox_zones ON ox_affiliates.affiliateid=ox_zones.affiliateid where delivery = %s and managed = %s""", (delivery,managed))
    managed_affiliates=c.fetchall()
    affiliates_zones = dict(managed_affiliates)
    return affiliates_zones
#(7, 11)  //(affiliateid,zoneid)

def zone_affiliateid(zoneid):
    try :
        affid = affiliate_zone.keys()[affiliate_zone.values().index(zoneid)]
    except :
        affid = 0

    return affid

def ref_camp(campaignid):
    try:
        campaignId = ref_camp[campaignid]
    except :
        campaignId = campaignid
    return campaignId

#currentDate is yesterday and formatted_previousDate is day before yesterday
# currentDate = time.strftime("%Y-%m-%d")
# read_date = datetime.datetime.strptime(currentDate, "%Y-%m-%d")
# previous_day = read_date - datetime.timedelta(days=1)
# formatted_previous_day = previous_day.strftime('%Y-%m-%d')
# currentDate = formatted_previous_day
# previousDate = datetime.datetime.strptime(currentDate, "%Y-%m-%d") - datetime.timedelta(days=1)
# formatted_previousDate = previousDate.strftime('%Y-%m-%d')

data = {}

def optionaladver(document,date,project_id,dataset_id,table_name,writer) :
    # Grab the application's default credentials from the environment.
    credentials = GoogleCredentials.get_application_default()
    # Construct the service object for interacting with the BigQuery API.
    bigquery_service = build('bigquery', 'v2', credentials=credentials)
    # Construct the service object for interacting with the BigQuery API.
    bigquery = discovery.build('bigquery', 'v2', credentials=credentials)
    #affiliate_zone = fetch_managed_affiliates()
    try:
        product_id = document["optionaladver"]
    except:
        product_id=''
    product_id = product_id.strip('[]')
    products=product_id.split(',')
    products=products[0].split('^')
    campaignId  = ref_camp(document["campaignid"])
    count = 0

    while (count < len(products)):  
        try:
            data['impressions'] = 0
            data['clicks'] = 0
            data['firstclick'] = 0
            data['conversion'] = 1
            data['dateTime'] = document["interval_start"]
            data['affiliateId'] = zone_affiliateid(document["zone_id"])
            if document["cookieId"] == None:
                data['userTrackingCookie']= 'null'
            else :
                data['userTrackingCookie'] = document["cookieId"]
            data['campaignId'] = campaignId
            data['productId'] = products[count]           
            #json_data = json.dumps(data)
            #print "json data is ",json_data 
            try:
                flag = True
                for key in data.keys():
                    if data[key] == "":
                        flag = False
                        break
                if flag == True:
                    print(data)
                    writer.writerow(data)
                    #stream_row_to_bigquery(bigquery, project_id, dataset_id, table_name, data, num_retries=5)
                    print "sucessfully inserted"
            except Exception, e:
                    print e
                    pass

            count = count + 1
        except Exception, e:
            print "data format is not correct"

def event_item_json(document,date,project_id,dataset_id,table_name,writer) :

    # Grab the application's default credentials from the environment.
    credentials = GoogleCredentials.get_application_default()
    # Construct the service object for interacting with the BigQuery API.
    bigquery_service = build('bigquery', 'v2', credentials=credentials)
    # Construct the service object for interacting with the BigQuery API.
    bigquery = discovery.build('bigquery', 'v2', credentials=credentials)

    campaignId  = ref_camp(document["campaignid"])
    data['impressions'] = 0
    data['clicks'] = 0
    data['firstclick'] = 0
    data['conversion'] = 1
    data['dateTime'] = document["interval_start"]
    data['affiliateId'] = document["affiliateid"]
    data['campaignId'] = campaignId   
    data['userTrackingCookie'] = document["userTrackingCookie"]


        #data['source'] = 'et'
    # json_data = json.dumps(data)
    # print "json data is ",json_data 
    try:
        product_id = document["stdEventItemJson"][0]
        data['productId'] = product_id['2']
        print data
        writer.writerow(data)
        #stream_row_to_bigquery(bigquery, project_id, dataset_id, table_name, data, num_retries=5)
        print "sucessfully inserted"
    except Exception, e:
        print "documnet is not correct"

    

def conversionData(formatted_previousDate,currentDate,project_id,dataset_id,table_name,writer):
    #time is added in query
    formatted_time = " 18:29:59"

    #mongo query 
    cursor = db.ox_raw_conversions.find({"stdEventId" : 62,  "stdEventItemJson": { "$exists": "false" } , "interval_start": {"$gte": formatted_previousDate+formatted_time, "$lt": currentDate+formatted_time}})
    print currentDate
    print "ET Data count is : "
    for document in cursor:
       event_item_json(document,currentDate,project_id,dataset_id,table_name,writer)


def containerData(formatted_previousDate,currentDate,project_id,dataset_id,table_name,writer):
    #time is added in query
    formatted_time = " 18:29:59"

    cursor = db.ox_raw_conversions.find({"campaignid" : {"$in" : container_campaign_id },"optionaladver":{"$ne" : "optional_val"},"interval_start": {"$gte": formatted_previousDate+formatted_time, "$lt": currentDate+formatted_time}})
    print cursor
    print "Container Data count is : "
    for document in cursor:
        optionaladver(document,currentDate,project_id,dataset_id,table_name,writer)


if __name__ == '__main__':
    currentDate = time.strftime("%Y-%m-%d")

    totalDays = 2

    parser = argparse.ArgumentParser(
                    description=__doc__,
                    formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('project_id', help='tyroobackup')
    parser.add_argument('dataset_id', help='openx')
    parser.add_argument(
                    'table_name', help='ecpmPLAData')
    parser.add_argument(
                    '-p', '--poll_interval',
                    help='How often to poll the query for completion (seconds).',
                    type=int,
                    default=1)
    parser.add_argument(
                    '-r', '--num_retries',
                    help='Number of times to retry in case of 500 error.',
                    type=int,
                    default=5)
    args = parser.parse_args()

    # with open('et_data100.csv', 'w') as csvfile:
    #     fieldnames = ['impressions','clicks','firstclick','conversion','dateTime','affiliateId','userTrackingCookie','campaignId','productId']
    #     writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    #     print writer
    #     for i in range(1,totalDays):
    #                     read_date = datetime.datetime.strptime(currentDate, "%Y-%m-%d")
    #                     previous_day = read_date - datetime.timedelta(days=1)
    #                     formatted_previous_day = previous_day.strftime('%Y-%m-%d')
    #                     currentDate = formatted_previous_day
    #                     previousDate = datetime.datetime.strptime(currentDate, "%Y-%m-%d") - datetime.timedelta(days=1)
    #                     formatted_previousDate = previousDate.strftime('%Y-%m-%d')
    #                     affiliate_zone = affiliates_zones()
    #                     conversionData(formatted_previousDate,currentDate,args.project_id,args.dataset_id,args.table_name,writer)
                        
    with open('container_data100.csv', 'w') as csvfile:
        fieldnames = ['impressions','clicks','firstclick','conversion','dateTime','affiliateId','userTrackingCookie','campaignId','productId']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        print writer
        for i in range(1,totalDays):
                        read_date = datetime.datetime.strptime(currentDate, "%Y-%m-%d")
                        previous_day = read_date - datetime.timedelta(days=1)
                        formatted_previous_day = previous_day.strftime('%Y-%m-%d')
                        currentDate = formatted_previous_day
                        previousDate = datetime.datetime.strptime(currentDate, "%Y-%m-%d") - datetime.timedelta(days=1)
                        formatted_previousDate = previousDate.strftime('%Y-%m-%d')
                        affiliate_zone = affiliates_zones()
                        containerData(formatted_previousDate,currentDate,args.project_id,args.dataset_id,args.table_name,writer)

