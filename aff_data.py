import time
from datetime import timedelta
import datetime
import json
import MySQLdb
import argparse
import csv
import subprocess
import os
import uuid


def affiliates() :
    dbSql=MySQLdb.connect(host='52.74.245.229',user='root',passwd='svgx1214tyr004777cm',db='openx')
    c=dbSql.cursor()
    managed ='1'
    c.execute("""SELECT ox_affiliates.affiliateid FROM ox_affiliates  where  managed = %s""", (managed))
    managed_affiliates=c.fetchall()
    return managed_affiliates
#(7, 11)  //(affiliateid,zoneid)


def aff_data(table_name,formatted_previousDate,currentDate,affiliate):
    #cmd = 'bq query --nosync --destination_table=openx.ecpmPlaDataTemp%s "SELECT count(*) as impressions,0 as clicks,0 as firstclicks, 0 as conversion,dateTime,affiliateId,userTrackingCookie,campaignId,productId FROM [userstory.userstory] where event = 99991 and productId is NOT NULL and dateTime>"%s 18:29:59"  and dateTime<"%s 18:29:59" group by  affiliateId,userTrackingCookie,campaignId,productId,dateTime"' %s (table_name)
    query ="SELECT impressions as impressions,clicks as clicks,firstclick as fclicks, conversion as conversions,dateTime,affiliateId,userTrackingCookie as userid,campaignId as campaign_id,productId as product_id FROM [openx.ecpm_pla_data] where affiliateId = %s and productId is NOT NULL and dateTime>'%s 18:29:59' and dateTime<'%s 18:29:59' order by productId" % (affiliate,formatted_previousDate,currentDate)
    cmd = 'bq query --nosync --allow_large_results --destination_table=ecpm_pla_data.%s' % (table_name) + ' "%s"' % (query)
    print cmd
    proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    print "program output:", out
    jobid = out.split(' ')
    print jobid[3]
    attempt = 0 
    while True:
        cmd = 'bq show --format=prettyjson -j %s| awk '"'{print $2}'"' | grep "DONE"' %(jobid[3].rstrip())
        proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        if out:  
            print("job completed")
            #export table to csv on google bucket
            #time.sleep(5)
            cmd ='python export_csv.py tyroobackup ecpm_pla_data %s gs://ecpmpladata/%s.csv' %(table_name,table_name)
            proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            print "program output:", out
            #time.sleep(5)
            #download  csv 
            cmd ='gsutil cp gs://ecpmpladata/%s.csv /home/ec2-user/pub-recommender/trackingData/' %(table_name)
            proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            print "program output:", out
            break
        else:
            print("job still pending! try again")
            attempt = attempt + 1
            if attempt == 20:
                break
            time.sleep( 5 )


if __name__ == '__main__':
    currentDate = time.strftime("%Y-%m-%d")
    read_date = datetime.datetime.strptime(currentDate, "%Y-%m-%d")
    previous_day = read_date - datetime.timedelta(days=1)
    currentDate = previous_day.strftime('%Y-%m-%d')
    
    aff_id = affiliates()
    print aff_id
    for i in range(0,len(aff_id)):
        affId = aff_id[i]
        date = time.strftime("%Y%m%d")
        table_name='pladata'+date+'_'+str(int(''.join(map(str, affId))))

        print table_name

        previousDate = datetime.datetime.strptime(currentDate, "%Y-%m-%d") - datetime.timedelta(days=90)
        formatted_previousDate = previousDate.strftime('%Y-%m-%d')
        affiliate = str(int(''.join(map(str, affId))))
        print aff_data(table_name,formatted_previousDate,currentDate,affiliate)

