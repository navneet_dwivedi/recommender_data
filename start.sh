#!/bin/bash
cd /home/ec2-user/recommender_data
echo "data collection started for $(date)"

python container_data.py tyroobackup openx ecpm_pla_data

echo "csv of container data is ready to upload in ecpm_pla_data"

bq load --skip_leading_rows=1 openx.ecpm_pla_data container_data100.csv impressions:integer,clicks:integer,firstclick:integer,conversion:integer,dateTime:timestamp,affiliateId:integer,userTrackingCookie:string,campaignId:integer,productId:string

echo "container_data100.csv uploaded sucessfully"

python et_data.py tyroobackup openx ecpm_pla_data

echo "csv of et data is ready to upload in ecpm_pla_data"

bq load --skip_leading_rows=1 openx.ecpm_pla_data et_data100.csv impressions:integer,clicks:integer,firstclick:integer,conversion:integer,dateTime:timestamp,affiliateId:integer,userTrackingCookie:string,campaignId:integer,productId:string

echo "et_data100.csv is uploaded sucessfully"

python user_story.py

echo "user story data is uploaded in ecpm_pla_data"

rm -f /home/ec2-user/pub-recommender/trackingData/*.csv

echo "script is ready to download csv affiliate wise"

python aff_data.py

echo "all files downloaded sucessfully"

rm -f /home/ec2-user/pub-recommender/trackingData/*15927.csv
rm -f /home/ec2-user/pub-recommender/trackingData/*1907.csv
rm -f /home/ec2-user/pub-recommender/trackingData/*7.csv

echo "fail over for affiliate 19037"

python fail_over.py

sed -i 1d /home/ec2-user/pub-recommender/trackingData/pladata*_19037_1.csv 
sed -i 1d /home/ec2-user/pub-recommender/trackingData/pladata*_19037_2.csv 
sed -i 1d /home/ec2-user/pub-recommender/trackingData/pladata*_19037_3.csv 
sed -i 1d /home/ec2-user/pub-recommender/trackingData/pladata*_19037_4.csv

cat  /home/ec2-user/pub-recommender/trackingData/*_19037_0.csv /home/ec2-user/pub-recommender/trackingData/*_19037_1.csv /home/ec2-user/pub-recommender/trackingData/*_19037_2.csv /home/ec2-user/pub-recommender/trackingData/*_19037_3.csv /home/ec2-user/pub-recommender/trackingData/*_19037_4.csv > /home/ec2-user/pub-recommender/trackingData/pladata_19037.csv

rm -f /home/ec2-user/pub-recommender/trackingData/*_19037_*.csv

echo "fail over cron is completed"

curl https://slack.com/api/chat.postMessage -X POST -d 'token=xoxp-c075e55d6d&channel=#recommender_logs&text=data is synced in ecpm_pla_data, csv is downloaded in trackingData.&username=logger'
